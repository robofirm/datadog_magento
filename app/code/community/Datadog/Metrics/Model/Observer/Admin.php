<?php

class Datadog_Metrics_Model_Observer_Admin
{
    private function getCurrentAdminUser(){
        $adminUser = Mage::getSingleton('admin/session')->getUser();
        if(is_object($adminUser)){
            // This should be a logged in admin user
            return Mage::getSingleton('admin/session')->getUser()->getUsername();
        }
        return "none";
    }
    public function cleanCacheType(Varien_Event_Observer $observer)
    {
        $cache_type = $observer->getData('type');
        $key = $key = 'magento.cache.refresh.all';
        $tags = array();
        $tags['magento.admin.username'] = $this->getCurrentAdminUser();
        $tags['magento.cache_type'] = $cache_type;
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        
        $queue->addMessage($key,[],$tags);        

    }

    public function cleanCacheAll(Varien_Event_Observer $observer)
    {
        $key = $key = 'magento.cache.flush.storage';
        $tags = array();
        $tags['magento.admin.username'] = $this->getCurrentAdminUser();
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,array(),$tags);
    }   
    
    public function cleanCacheSystem(Varien_Event_Observer $observer)
    {
        $key = $key = 'magento.cache.flush.system';
        $tags = array();
        $tags['magento.admin.username'] = $this->getCurrentAdminUser();
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,[],$tags);
    }   
    
    public function cleanCacheMedia(Varien_Event_Observer $observer)
    {
        $key = $key = 'magento.cache.flush.media';
        $tags = array();
        $tags['magento.admin.username'] = $this->getCurrentAdminUser();
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,[],$tags);
    }   
    
    public function reindexMedia(Varien_Event_Observer $observer)
    {
        $key = $key = 'magento.cache.flush.js_css';
        $tags = array();
        $tags['magento.admin.username'] = $this->getCurrentAdminUser();
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,[],$tags);
    }   
    
    public function userLogin(Varien_Event_Observer $observer)
    {
        $key = 'magento.admin.login';
        $tags = array();
        $tags['magento.admin.username'] = $observer->getEvent()->getUsername();
        $tags['magento.admin.login.status'] = $observer->getEvent()->getResult() ? "true":"false";
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,[],$tags);
    }   
    public function reindexProcess(Varien_Event_Observer $observer)
    {

       $requestParams =  $observer->getControllerAction()->getRequest()->getParams();
       $process = Mage::getSingleton('index/indexer')->getProcessById($requestParams['process']);

        
        $key = 'magento.index.reindex';
        $tags = array();
        $tags['magento.index.name'] = $process->getIndexerCode();
        $tags['magento.admin.username'] = $this->getCurrentAdminUser();
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,[],$tags);

    }   
    
    
}