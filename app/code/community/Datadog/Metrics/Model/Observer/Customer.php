<?php

class Datadog_Metrics_Model_Observer_Customer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function customerCustomerAuthenticated(Varien_Event_Observer $observer)
    {
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage('magento.customer.login');

        return;
    }
}