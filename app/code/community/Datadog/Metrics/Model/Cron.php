<?php

class Datadog_Metrics_Model_Cron
{
    public function collect(){
        // Instance the queue for sending out messages
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        
        // Cron Queue Stats
        $cronQueueSize =  Mage::getModel('cron/schedule')->getCollection()
        ->addFieldToFilter('scheduled_at', array('lt' => strftime('%Y-%m-%d %H:%M:%S', time())))
        ->addOrder('scheduled_at', 'ASC')->getSize();
        
        $cronQueuePendingSize =  Mage::getModel('cron/schedule')->getCollection()
        ->addFieldToFilter('status', "pending")
        ->addFieldToFilter('scheduled_at', array('lt' => strftime('%Y-%m-%d %H:%M:%S', time())))
        ->addOrder('scheduled_at', 'ASC')->getSize();

        $cronQueueDiedSize =  Mage::getModel('cron/schedule')->getCollection()
        ->addFieldToFilter('status', "died")
        ->addFieldToFilter('scheduled_at', array('lt' => strftime('%Y-%m-%d %H:%M:%S', time())))
        ->addOrder('scheduled_at', 'ASC')->getSize();        

        
        $cronQueueErrorSize =  Mage::getModel('cron/schedule')->getCollection()
        ->addFieldToFilter('status', "error")
        ->addFieldToFilter('scheduled_at', array('lt' => strftime('%Y-%m-%d %H:%M:%S', time())))
        ->addOrder('scheduled_at', 'ASC')->getSize();
        
        $cronQueueSuccessSize =  Mage::getModel('cron/schedule')->getCollection()
        ->addFieldToFilter('status', "success")
        ->addFieldToFilter('scheduled_at', array('lt' => strftime('%Y-%m-%d %H:%M:%S', time())))
        ->addOrder('scheduled_at', 'ASC')->getSize();        

        # send out the metrics
        $queue->addMessage('magento.cron.queue.total', $cronQueueSize, [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        $queue->addMessage('magento.cron.queue.pending', $cronQueuePendingSize, [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        $queue->addMessage('magento.cron.queue.died', $cronQueueDiedSize, [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        $queue->addMessage('magento.cron.queue.error', $cronQueueErrorSize, [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        $queue->addMessage('magento.cron.queue.success', $cronQueueSuccessSize, [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        
        
        // Online Users:
        $_visitors = Mage::getModel('log/visitor_online');
        $visitor_count = $_visitors->prepare()->getCollection()->count();
        $queue->addMessage('magento.visitors.online', $visitor_count, [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        
        
        // Sales Totals Today
        $_sales = Mage::getModel('sales/order')->getCollection()
            ->addAttributeToFilter('created_at', array('from' => date('Y-m-d'), 'to' => date('Y-m-d', strtotime('+1 days'))));
        $_sales->getSelect()
            ->reset(Zend_Db_Select::COLUMNS) //remove existings selects
            ->columns(array('grand_total' => new Zend_Db_Expr('SUM(grand_total)'))) //add expresion to sum the grand_total
            ->columns(array('count' => new Zend_Db_Expr('COUNT(entity_id)'))) //add expression to count the orders
            ->columns(array('shipping_total' => new Zend_Db_Expr('SUM(shipping_amount+shipping_tax_amount)'))); //add expression to calculate the shipping costs
        $todaySalesData = $_sales->getFirstItem(); //because this is a collection, we need just the first item.
        
        # Send out metrics as messages
        $queue->addMessage('magento.sales.today.count', $todaySalesData['count'], [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        $queue->addMessage('magento.sales.today.grand_total', $todaySalesData['grand_total'], [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        $queue->addMessage('magento.sales.today.shipping_total', $todaySalesData['shipping_total'], [], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
         
                
        // Indexer Status
        $indexProcessCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();
        $key = 'magento.index.status';       
        foreach($indexProcessCollection as $indexProcess){
            $tags = array(
                "magento.index.code"=>$indexProcess->getIndexerCode(),   
            );
            if($indexProcess->getStatus()==Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX){
                $indexerStatus = 0;
            }else{
                $indexerStatus = 1;
            }            
            $queue->addMessage($key, $indexerStatus, $tags, Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        }
      
        
        // Cache Status
        $cacheTypes = Mage::app()->getCacheInstance()->getTypes();
        $cacheTypesDirty = Mage::app()->getCacheInstance()->getInvalidatedTypes();
        $key = 'magento.cache.status';
        foreach($cacheTypes as $cacheCode => $cacheInfo){
            $tags = array(
                "magento.cache.code"=>$cacheCode,
            );            
           $cacheClean = array_key_exists($cacheCode,$cacheTypesDirty) ? 0 : 1;
           if($cacheInfo['status'] == 0){
               // Cache Disabled, send a -1 for status
               $cacheStatus = -1;
           }else{
               // Cache is enabled
               if($cacheClean == 1){
                   // Cache Clean, send a 1 for status
                    $cacheStatus = 1;
               }else{
                   // Cache Dirty, send a 0 for status
                    $cacheStatus = 0;       
               }               
           }
           $queue->addMessage($key, $cacheStatus, $tags, Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE);
        }

        // Send messages, because you can't count on destruct in this case.
        $queue->sendMessages();
        
    }
    
}