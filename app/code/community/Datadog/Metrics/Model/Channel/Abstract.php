<?php
/**
 * This file was part of Datadog_Metrics for Magento.
 * (Modified and used as part of the updated datadog integration)
 *
 * @license OSL v3
 * @author Jacques Bodin-Hullin <j.bodinhullin@monsieurbiz.com> <@jacquesbh>
 * @category Hackathon
 * @package Datadog_Metrics
 * @copyright Copyright (c) 2014 Magento Hackathon (http://mage-hackathon.de)
 */

/**
 * Channel_Abstract Model
 * @package Datadog_Metrics
 */
class Datadog_Metrics_Model_Channel_Abstract
    extends Mage_Core_Model_Abstract
    implements Datadog_Metrics_Model_Channel_Interface
{

    /**
     * {@inheritdoc}
     */
    public function send($key, $value, array $tags, $type)
    {
        throw new RuntimeException("Please implement first!");
    }

}
