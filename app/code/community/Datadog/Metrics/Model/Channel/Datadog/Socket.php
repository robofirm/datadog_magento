<?php
/**
 * This file was part of Datadog_Metrics for Magento.
 * (Modified and used as part of the updated datadog integration)
 *
 * @license OSL v3
 * @author Jacques Bodin-Hullin <j.bodinhullin@monsieurbiz.com> <@jacquesbh>
 * @category Hackathon
 * @package Datadog_Metrics
 * @copyright Copyright (c) 2014 Magento Hackathon (http://mage-hackathon.de)
 */

require_once 'Datadog/Datadogstatsd.php';

/**
 * Channel_Datadog_Socket Model
 * @package Datadog_Metrics
 */
class Datadog_Metrics_Model_Channel_Datadog_Socket
    extends Datadog_Metrics_Model_Channel_Abstract
    implements Datadog_Metrics_Model_Channel_Interface
{

    /**
     * {@inheritdoc}
     */
    public function send($key, $value, array $tags, $type)
    {
        switch ($type) {
            case Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_INCREMENT:
                Datadogstatsd::increment($key, 1, $tags);
                break;
            case Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_HISTOGRAM:
                Datadogstatsd::histogram($key, $value, 1, $tags);
                break;
            case Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_GAUGE:
                Datadogstatsd::gauge($key, $value, 1, $tags);
                break;
            case Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_TIMING:
                Datadogstatsd::timing($key, microtime(), 1, $tags);
                break;
            case Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_SET:
                Datadogstatsd::set($key, $value, 1, $tags);
                break;
            default:
                throw new ErrorException("This type doesn't exist.");
        }
        return $this;
    }

}